from socket import *
import threading
import random

import json
import solitaire
import merkleHellman

solitaireKey = [7, 2, 3, 4, 53, 6, 1, 8, 9, 10, 11, 12, 13, 14, 15, 17, 16, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 54, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 5, 40]

def generate_half_secret():
    cards = [x for x in range(1, 55) if x % 2 == 1]
    random.shuffle(cards)
    return cards

def receive(connectionSocket: socket, id):
    while True:
        response = connectionSocket.recv(1024)            
        
        msg = solitaire.decode(response, solitaireKey)
        if response == "quit":
            break
        print("\nreceived: " + msg + "\n")

def sendmsg(connectionSocket: socket, otherId):
    while True:
        msg = input()
        msg = solitaire.encode(msg, solitaireKey)
        json_to_send = json.dumps({"command": "msg", "id": otherId})
        connectionSocket.send(json_to_send.encode())
        connectionSocket.send(msg)
        if msg == 'quit':
            break


def main():
    global alive
    serverSocket = socket(AF_INET, SOCK_STREAM)
    serverSocket.connect(("localhost", 8000))

    private_key = merkleHellman.generatePrivateKey()
    public_key = merkleHellman.generatePublicKey(private_key)

    myId = input("A felhasznalod: ")

    json_to_send = json.dumps({"command": "registration", "id": myId, "public_key": public_key})
    serverSocket.send(json_to_send.encode())

    json_got = json.loads(serverSocket.recv(1024).decode())
    print("user registered with id =", myId)

    otherId = input("A masik felhasznalo id-ja: ")

    json_to_send = json.dumps({"command": "get_public_key", "id": otherId})
    serverSocket.send(json_to_send.encode())

    json_got = json.loads(serverSocket.recv(1024).decode())

    print("public key of client =", otherId, json_got["public_key"])

    szalacska = threading.Thread(target=sendmsg,args=(serverSocket,otherId))
    szalacska.start()

    szalacska2 = threading.Thread(target=receive,args=(serverSocket,otherId))
    szalacska2.start()
    szalacska2.join()

main()