import merkleHellman

def main():

    msg = "szia"
    print("The message: ", msg)

    (w,q,r) = merkleHellman.generatePrivateKey()
    print("Private key elements: w: ", w, ", q: ", q, ", r: ", r)

    publicKey = merkleHellman.generatePublicKey((w,q,r))
    print("Public key: ", publicKey)

    encryptedMessage = merkleHellman.encrypt(msg, publicKey)
    print("Encrypted message: ", encryptedMessage)

    decryptedMessage = merkleHellman.decrypt(encryptedMessage, (w,q,r))
    print("Decrypted message: ", decryptedMessage)
    
    if msg == decryptedMessage:
        print("The algorithm runs successfully.")
    else:
        print("There is some error in the algorithm.")

if __name__ == "__main__":
    main()