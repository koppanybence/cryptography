import solitaire

def main():

    msg = "szia"
    print("The message: ", msg)

    encryptedMessage = solitaire.encode(msg, [7, 2, 3, 4, 53, 6, 1, 8, 9, 10, 11, 12, 13, 14, 15, 17, 16, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 54, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 5, 40])
    print("Encrypted message: ", encryptedMessage)

    decryptedMessage = solitaire.decode(encryptedMessage, [7, 2, 3, 4, 53, 6, 1, 8, 9, 10, 11, 12, 13, 14, 15, 17, 16, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 54, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 5, 40])
    print("Decrypted message: ", decryptedMessage)
    
    if msg == decryptedMessage:
        print("The algorithm runs successfully.")
    else:
        print("There is some error in the algorithm.")

if __name__ == "__main__":
    main()