import random
import math

def coPrime(a, b):
    return math.gcd(a, b) == 1

def convertCharacterToBits(character):
    temp = bin(ord(character)).replace('b', '')

    tempArr = [int(numeric_string) for numeric_string in temp]
    if(len(tempArr) < 8):
        for x in range(8 - len(tempArr)):
            tempArr.insert(0,0)

    return tempArr

def convertToOriginalCharacter(s, w):
    bytesArr = []
    for wi in range(7, -1, -1):
        if w[wi] <= s:
            bytesArr.append(1)
            s -= w[wi]
        else:
            bytesArr.append(0)
    
    sum = 0
    y = 1
    for x in bytesArr:
        sum += x * y
        y = y * 2

    return sum

def generatePrivateKey(n = 8):
    v = []
    v.append(random.randint(1,10))
    sum = v[0]

    for i in range(1,n):
        v.append(sum + random.randint(1,10))
        sum += v[i]

    m = random.randint(sum + 1, sum * 5)

    a = 0
    while not coPrime(a, m):
        a = random.randint(2, m - 1)

    return v, m, a

def generatePublicKey(privatekey):

    (v, m, a) = privatekey
    return [a * vi % m for vi in v]

def encrypt(msg, publicKey):

    encryptedMsg = []
    for i in range(len(msg)):
        bits = convertCharacterToBits(msg[i])

        print("bits: ", bits)
        print("publicKey: ", publicKey)

        print("lenBits: ", len(bits))
        print("lenPublicKey: ", len(publicKey))
        print("a szar: ", [bits[j] * publicKey[j] for j in range(8)])
        
        encryptedMsg.append(sum([bits[j] * publicKey[j] for j in range(8)]))

    return encryptedMsg

def decrypt(msg, privateKey):
    (w, q, r) = privateKey

    r_inv = 0
    for i in range(1, q):
        if (r * i) % q == 1:
            r_inv = i
            break

    decodedMsg = ""
    for x in range(len(msg)):
        coded = msg[x] * r_inv % q
        decodedMsg += chr(convertToOriginalCharacter(coded, w))

    return decodedMsg