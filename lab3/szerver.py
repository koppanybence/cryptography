import threading
from socket import *

from socket import *
import threading
import json

users = []

def userAlreadyRegitrated(id):
    global users
    for user in users:
        if user.id == id:
            return True
    return False

class User:
    def __init__(self, id, userSocket, public_key):
        self.id = id
        self.userSocket = userSocket
        self.public_key = public_key

server_socket = None
felhasznalok = []
socketek = []

def thread(connection_socket):
    global users

    while True:
        jsonData = json.loads(connection_socket.recv(1024).decode())
        command = jsonData["command"]
        if command == "get_public_key":
                print('id =', jsonData["id"])
                key = []
                for user in users:
                    if user.id == jsonData["id"]:
                        key = user.public_key
                        break
                print('public key of user =', jsonData["id"], key)
                public_key = key
                json_to_send = json.dumps({"message": "ok", "public_key": public_key})
                connection_socket.send(json_to_send.encode())
        elif command == "msg":
            socket = 0
            for user in users:
                if user.id == jsonData["id"]:
                    socket_ok = user.userSocket
            rcv = connection_socket.recv(1024)
            socket_ok.send(rcv)
        elif command == "secret":
            socket = 0
            for user in users:
                if user.id == jsonData["id"]:
                    socket = user.userSocket
            socket.send(jsonData["msg"].encode())

def main():
    global server_socket, socketek
    server_socket = socket(AF_INET, SOCK_STREAM)
    server_socket.bind(("localhost", 8000))
    server_socket.listen(3)
    print("A szerver keszen all!")

    global users

    while True:
        connection_socket, addr = server_socket.accept()
        socketek.append(connection_socket)

        jsonData = json.loads(connection_socket.recv(1024).decode())
        print('json_got =', jsonData)


        command = jsonData["command"]
        print('command =', command)

        if command == "registration":
            client_id = jsonData["id"]
            client_socket = connection_socket
            client_public_key = jsonData["public_key"]

            if userAlreadyRegitrated(client_id) == False:
                newUser = User(client_id, client_socket, client_public_key)
                users.append(newUser)
                print('user registered with id =', client_id)
                json_to_send = json.dumps({"message": "registered"})
                connection_socket.send(json_to_send.encode())
            else:
                for user in users:
                    if user.id == jsonData["id"]:
                        user.public_key = client_public_key
                        print('client wit id updated its public key', client_id)
                        json_to_send = json.dumps({"message": "public key updated"})
                        connection_socket.send(json_to_send.encode())
                        break

        threading.Thread(target=thread, args=(connection_socket, )).start()

main()
