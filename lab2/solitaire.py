def solitaireKeyGenerator(key):
    while True:
        #seacrhing for the white joker
        whiteJoker = key.index(53)
        if whiteJoker == 53:
            key = [key[0]] + [53] + key[1:53]
        else:
            key[whiteJoker], key[whiteJoker+1] = key[whiteJoker+1], key[whiteJoker] 
                
        #searching for the black joker
        blackJoker = key.index(54)
        if blackJoker == 53:
            key = [key[0]] + [key[1]] + [54] + key[2:53]
        elif blackJoker == 52:
            key = [key[0]] + [54] + key[1:52] + [key[53]]
        else:
            key[blackJoker], key[blackJoker+1], key[blackJoker + 2] = key[blackJoker + 1], key[blackJoker + 2], key[blackJoker]
    
        #swapping the deck
        joker1 = key.index(53)
        joker2 = key.index(54)
        if joker1 > joker2:
            joker1, joker2 = joker2, joker1
        
        key = key[(joker2 + 1): 54] + key[joker1 : (joker2 + 1)] + key[0 : joker1] 

        #swapping for 2nd time
        lastElement = key[53]

        if((lastElement != 53) and (lastElement != 54)):
            key = key[lastElement:53] + key[0:lastElement] + [key[53]]

        firstElement = key[0]
        if firstElement < 53 :
            return key[firstElement], key

def keyStreamGenerator(messageLength, key):

    result = b''
    for i in range(messageLength):
        (value1, key) = solitaireKeyGenerator(key)
        (value2, key) = solitaireKeyGenerator(key)

        key_byte = bytes([(value1*value2) & 255])
        result += key_byte

    return result