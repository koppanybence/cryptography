def generateValueX(x):
    x = pow(x, 2) % n
    return x

def generateBits(amount):
    generatedValues = ""
    x = pow(seed,2) % n
    for i in range(0, amount):
        x = generateValueX(x)
        generatedValues += str(x % 2)
    return generatedValues

def keyStreamGenerator(messageLength, key):

    p = key[0]
    q = key[1]
    global n 
    n = p*q
    global seed
    seed = key[2]
    x = 0

    finalResult = b''
    result = generateBits(messageLength*8)
    newData = [int(result[x:x+8], 2) for x in range(0, len(result), 8)]

    for i in range(0, messageLength):
        key_byte = bytes([newData[i]])
        finalResult += key_byte

    return finalResult