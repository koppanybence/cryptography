from socket import *
import threading
import json
import flowCrypt
import sys

global quitting 
global clientSocket
global client2Socket

def read_json():
    with open("config.json", "r") as f:
        return json.load(f)

def listen(encrypter, seed):
    global clientSocket
    global client2Socket
    global quitting
    recvSocket, addr = clientSocket.accept()
    
    while True:
        message = recvSocket.recv(1024)
        message = flowCrypt.decode(encrypter, message,  seed)
        if message == "QUIT":
            if(quitting == False):
                secretMessage = flowCrypt.encode(encrypter, "QUIT",  seed)
                client2Socket.send(secretMessage)
                print("Your partner has quitted... type QUIT for ending the conversation. ")
            break
        print('[client]: ', message)
    sys.exit()

def _get_selection(prompt, options):
    choice = input(prompt).upper()
    while not choice or choice[0] not in options:
        choice = input("Please enter one of {}. {}".format('/'.join(options), prompt)).upper()
    return choice[0]

def main():
    
    global quitting 
    global clientSocket
    global client2Socket

    quitting = False
    encrypter = ''
    config = read_json()

    print("You are connected. Type QUIT, if you want to end the conversation...")
    choice = _get_selection("(S)olitaire or (B)lum-Blum-Shub? ", "SB")
    if choice == 'S':
        encrypter = 'S'
        seed = config['solitaire']
    else:
        encrypter = 'B'
        seed = config['bbs']

    clientSocket=socket(AF_INET,SOCK_STREAM)
    clientSocket.bind(('localhost', 4567))
    clientSocket.listen(1)

    client2Socket = socket(AF_INET,SOCK_STREAM)

    threading.Thread(target=listen, args=(encrypter, seed)).start()

    print("Hit the enter button, if both sides are ready for chatting.")
    msg = input()

    client2Socket.connect(('localhost', 4568))
    print("You can start messaging...")

    while True:
        message = input()
        secretMessage = flowCrypt.encode(encrypter, message,  seed)
        client2Socket.send(secretMessage)
        if(message == "QUIT" and quitting == False):
            quitting = True
            break
        elif(message == "QUIT"):
            break

if __name__ == "__main__":
    main()