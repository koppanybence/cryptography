import solitaire
import blumBlumShub

def xorOperation(message_binary, keyStream):
    result = b""
    for i in range(len(message_binary)):
        result += bytes([message_binary[i] ^ keyStream[i]])
    return result

def encode(generatorType, message, key):
    if generatorType == 'S':
        keyStream = solitaire.keyStreamGenerator(len(message), key)
        return xorOperation(bytes(message, 'utf-8'), keyStream)
    else:
        keyStream = blumBlumShub.keyStreamGenerator(len(message), key)
        return xorOperation(bytes(message, 'utf-8'), keyStream)

def decode(generatorType, message, key): 
    if generatorType == 'S':
        keyStream = solitaire.keyStreamGenerator(len(message), key)
        return xorOperation(message, keyStream).decode("utf-8")
    else:
        keyStream = blumBlumShub.keyStreamGenerator(len(message), key)
        return xorOperation(message, keyStream).decode("utf-8")

def main():
    res = encode("S", "testinputsolitaire", [1, 2, 3, 9, 53, 6, 7, 8, 4, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 54, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 5, 40])
    res1 = decode("S", res, [1, 2, 3, 9, 53, 6, 7, 8, 4, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 54, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 5, 40])
    print(res1) 

    res = encode("B", "testinputbbs", [19, 11, 100])
    res1 = decode("B", res, [19, 11, 100])
    print(res1)

if __name__ == "__main__":
    main()