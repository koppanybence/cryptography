#!/usr/bin/env python3 -tt
"""
File: crypto.py
---------------
Assignment 1: Cryptography
Course: CS 41
Name: <YOUR NAME>
SUNet: <SUNet ID>

Replace this with a description of the program.
"""
import numpy

# Caesar Cipher

def encrypt_caesar(plaintext, binOrNot):
    if(binOrNot):
        chipertext = b''
        for letter in plaintext:
            chipertext = chipertext + (bytes([(letter + 3) % 256]))
        return chipertext
    ciphertext = "" 
    for letter in plaintext:
        if((ord(letter) > 90) or (ord(letter) < 65)):
            ciphertext = ciphertext + letter
        else:
            codedLetterNr = ord(letter) + 3
            if(codedLetterNr>90):
                codedLetterNr = codedLetterNr - 26
            ciphertext = ciphertext + chr(codedLetterNr)
    return ciphertext


def decrypt_caesar(ciphertext, binOrNot):
    if(binOrNot):
        plaintext = b''
        for letter in ciphertext:
            plaintext += bytes([(letter - 3) % 256])
        return plaintext 
    plaintext = ""
    for letter in ciphertext:
        if((ord(letter) > 90) or (ord(letter) < 65)):
            plaintext = plaintext + letter
        else:
            codedLetterNr = ord(letter) - 3
            if(codedLetterNr < 65):
                codedLetterNr = codedLetterNr + 26
            plaintext = plaintext + chr(codedLetterNr)
    return plaintext

# Vigenere Cipher

def encrypt_vigenere(plaintext, keyword):
    originalKeywordLenght = len(keyword)
    currentLetterOfKeyword = 0
    ciphertext = ""

    while(len(plaintext) != len(keyword)):
        if(currentLetterOfKeyword == originalKeywordLenght):
            currentLetterOfKeyword = 0
        keyword = keyword + keyword[currentLetterOfKeyword]
        currentLetterOfKeyword = currentLetterOfKeyword + 1
    
    for x in range(0, len(plaintext)):
        codedLetterNr = ord(plaintext[x]) + ord(keyword[x]) - 65
        if(codedLetterNr>90):
            codedLetterNr = codedLetterNr - 26
        ciphertext = ciphertext + chr(codedLetterNr)
    return ciphertext


def decrypt_vigenere(ciphertext, keyword):
    originalKeywordLenght = len(keyword)
    currentLetterOfKeyword = 0
    plaintext = ""

    while(len(ciphertext) != len(keyword)):
        if(currentLetterOfKeyword == originalKeywordLenght):
            currentLetterOfKeyword = 0
        keyword = keyword + keyword[currentLetterOfKeyword]
        currentLetterOfKeyword = currentLetterOfKeyword + 1
    
    for x in range(0, len(ciphertext)):
        deCodedLetterNr = ord(ciphertext[x]) - ord(keyword[x]) + 65
        if(deCodedLetterNr < 65):
            deCodedLetterNr = deCodedLetterNr + 26
        plaintext = plaintext + chr(deCodedLetterNr)
    return plaintext

def encrypt_scytale(plaintext, circumference):
    wordLength = len(plaintext)
    chipertext = ""
    actIndex = 0
    nrRows, nrColumns = circumference,0
    if(((wordLength)%circumference) != 0):
        nrColumns = int((wordLength)/circumference) + 1
    else:
        nrColumns = int((wordLength)/circumference)
    arr = numpy.array([['.']*nrColumns]*nrRows)

    for x in range(0, nrColumns):
        for y in range(0,nrRows):              
            if(actIndex < wordLength):
                arr[y,x] = plaintext[actIndex]
                actIndex = actIndex + 1

    for x in range(0,nrRows):
        for y in range(0, nrColumns):
            chipertext = chipertext + arr[x,y]

    return chipertext

def decrypt_scytale(chipertext, circumference):
    plaintext = ""
    wordLength = len(chipertext)
    actIndex = 0
    nrRows, nrColumns = circumference,0
    nrColumns = int((wordLength)/circumference)
    arr = numpy.array([['.']*nrColumns]*nrRows)

    for x in range(0, nrRows):
        for y in range(0,nrColumns):              
            arr[x, y] = chipertext[actIndex]
            actIndex = actIndex + 1

    for x in range(0, nrColumns):
        for y in range(0,nrRows):     
            if(arr[y, x] != "."):
                plaintext = plaintext + arr[y,x]

    return plaintext

def encrypt_railfence(plaintext, num_rails):
    chipertext = ""
    wordLength = len(plaintext)
    direction = True
    rowPosition, columnPosition = 0,0
    arr = numpy.array([['.']*wordLength]*num_rails)

    while(columnPosition < wordLength):
        arr[rowPosition, columnPosition] = plaintext[columnPosition]
        columnPosition = columnPosition + 1
        if(rowPosition == 2):
            direction = False
        if(rowPosition == 0):
            direction = True
        if(direction):
            rowPosition = rowPosition + 1
        else:
            rowPosition = rowPosition - 1

    for x in range(0,num_rails):
        for y in range(0,wordLength):
            if(arr[x][y] != "."):
                chipertext = chipertext + arr[x,y]

    return chipertext

def decrypt_railfence(ciphertext, num_rails):
    plaintext = ""
    wordLength = len(ciphertext)
    direction = True
    rowPosition, columnPosition = 0,0
    arr = numpy.array([['.']*wordLength]*num_rails)

    while(columnPosition < wordLength):
        arr[rowPosition, columnPosition] = "-"
        columnPosition = columnPosition + 1
        if(rowPosition == 2):
            direction = False
        if(rowPosition == 0):
            direction = True
        if(direction):
            rowPosition = rowPosition + 1
        else:
            rowPosition = rowPosition - 1

    positionInWord = 0
    for x in range(0,num_rails):
        for y in range(0,wordLength):
            if(arr[x, y] == "-"):
                arr[x, y] = ciphertext[positionInWord]
                positionInWord = positionInWord + 1

    rowPosition, columnPosition = 0,0
    while(columnPosition < wordLength):
        plaintext = plaintext + arr[rowPosition, columnPosition]
        columnPosition = columnPosition + 1
        if(rowPosition == 2):
            direction = False
        if(rowPosition == 0):
            direction = True
        if(direction):
            rowPosition = rowPosition + 1
        else:
            rowPosition = rowPosition - 1

    return plaintext

# Merkle-Hellman Knapsack Cryptosystem

def generate_private_key(n=8):
    """Generate a private key for use in the Merkle-Hellman Knapsack Cryptosystem.

    Following the instructions in the handout, construct the private key components
    of the MH Cryptosystem. This consistutes 3 tasks:

    1. Build a superincreasing sequence `w` of length n
        (Note: you can check if a sequence is superincreasing with `utils.is_superincreasing(seq)`)
    2. Choose some integer `q` greater than the sum of all elements in `w`
    3. Discover an integer `r` between 2 and q that is coprime to `q` (you can use utils.coprime)

    You'll need to use the random module for this function, which has been imported already

    Somehow, you'll have to return all of these values out of this function! Can we do that in Python?!

    @param n bitsize of message to send (default 8)
    @type n int

    @return 3-tuple `(w, q, r)`, with `w` a n-tuple, and q and r ints.
    """
    raise NotImplementedError  # Your implementation here

def create_public_key(private_key):
    """Create a public key corresponding to the given private key.

    To accomplish this, you only need to build and return `beta` as described in the handout.

        beta = (b_1, b_2, ..., b_n) where b_i = r × w_i mod q

    Hint: this can be written in one line using a list comprehension

    @param private_key The private key
    @type private_key 3-tuple `(w, q, r)`, with `w` a n-tuple, and q and r ints.

    @return n-tuple public key
    """
    raise NotImplementedError  # Your implementation here


def encrypt_mh(message, public_key):
    """Encrypt an outgoing message using a public key.

    1. Separate the message into chunks the size of the public key (in our case, fixed at 8)
    2. For each byte, determine the 8 bits (the `a_i`s) using `utils.byte_to_bits`
    3. Encrypt the 8 message bits by computing
         c = sum of a_i * b_i for i = 1 to n
    4. Return a list of the encrypted ciphertexts for each chunk in the message

    Hint: think about using `zip` at some point

    @param message The message to be encrypted
    @type message bytes
    @param public_key The public key of the desired recipient
    @type public_key n-tuple of ints

    @return list of ints representing encrypted bytes
    """
    raise NotImplementedError  # Your implementation here

def decrypt_mh(message, private_key):
    """Decrypt an incoming message using a private key

    1. Extract w, q, and r from the private key
    2. Compute s, the modular inverse of r mod q, using the
        Extended Euclidean algorithm (implemented at `utils.modinv(r, q)`)
    3. For each byte-sized chunk, compute
         c' = cs (mod q)
    4. Solve the superincreasing subset sum using c' and w to recover the original byte
    5. Reconsitite the encrypted bytes to get the original message back

    @param message Encrypted message chunks
    @type message list of ints
    @param private_key The private key of the recipient
    @type private_key 3-tuple of w, q, and r

    @return bytearray or str of decrypted characters
    """
    raise NotImplementedError  # Your implementation here